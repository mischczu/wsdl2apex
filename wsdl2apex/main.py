__author__ = 'bucholcm'
import argparse
from wsdl2apex import Wsdl2Apex

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', help='comma separated file names')
    parser.add_argument('-sw', '--startsWith', help='case sensitive beggining of file name')
    parser.add_argument('-d', '--dir', help='directory where files are located')
    args = parser.parse_args()

    wa = Wsdl2Apex(args)
    wa.execute()