__author__ = 'bucholcm'
import unittest
from testfixtures import TempDirectory
from wsdl2apex.wsdl2apex import Wsdl2Apex


class ArgparseMock():
    def __init__(self, directory, file_name, starts_with):
        self.dir = directory
        self.file = file_name
        self.startsWith = starts_with


class Wsdl2ApexTest(unittest.TestCase):
    src_str = 'public class aegonPlHighwayTypesAccount {\n'
    'public class CurrentAllocationAccountFundType {\n'
    'public Long fundId;\n'
    'public Integer splitPercent;\n'
    'private String[] fundId_type_info = new String[]{\'fundId\',\'http://aegon.pl/highway/types/account\',null,' \
        '\'1\',\'1\',\'false\'};\n'
    'private String[] splitPercent_type_info = new String[]{\'splitPercent\',\'http://aegon.pl/highway/types/account' \
        '\',null,\'1\',\'1\',\'false\'};\n'
    'private String[] apex_schema_type_info = new String[]{\'http://aegon.pl/highway/types/account\',\'true\',' \
        '\'false\'};\n'
    'private String[] field_order_type_info = new String[]{\'fundId\',\'splitPercent\'};\n'
    '}\n}\n'

    dst_str = 'public class aegonPlHighwayTypesAccount {\n'
    'public class CurrentAllocationAccountFundType {\n'
    'public Long fundId {get; set;}\n'
    'public Integer splitPercent{get; set;}\n'
    'private String[] fundId_type_info = new String[]{\'fundId\',\'http://aegon.pl/highway/types/account\',null,' \
        '\'1\',\'1\',\'false\'};\n'
    'private String[] splitPercent_type_info = new String[]{\'splitPercent\',\'http://aegon.pl/highway/types/account' \
        '\',null,\'1\',\'1\',\'false\'};\n'
    'private String[] apex_schema_type_info = new String[]{\'http://aegon.pl/highway/types/account\',\'true\',' \
        '\'false\'};\n'
    'private String[] field_order_type_info = new String[]{\'fundId\',\'splitPercent\'};\n'
    '}\n}\n'

    def test_replace_single_file(self):
        with TempDirectory() as d:
            d.write('test.txt', self.src_str)
            args = ArgparseMock(None, d.path + '/' + 'test.txt', None)
            w2a = Wsdl2Apex(args)
            w2a.execute()
            corrected_str = d.read('test.txt')
            self.assertEqual(self.dst_str, corrected_str)

    def test_replace_directory(self):
        with TempDirectory() as d:
            d.write('test1.txt', self.src_str)
            d.write('test2.txt', self.src_str)
            args = ArgparseMock(d.path, None, 'test')
            w2a = Wsdl2Apex(args)
            w2a.execute()
            corrected_str1 = d.read('test1.txt')
            corrected_str2 = d.read('test2.txt')

            self.assertEqual(self.dst_str, corrected_str1)
            self.assertEqual(self.dst_str, corrected_str2)


if __name__ == '__main__':
    unittest.main()