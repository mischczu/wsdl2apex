__author__ = 'bucholcm'
import os
import re
import sys
import fileinput

p = re.compile(r'public[\w\s]*;')


class Wsdl2Apex:

    def __init__(self, args):
        self.args = args
        self.files = []
        self.get_files()

    def execute(self):
        if len(self.files) > 0:
            for line in fileinput.input(self.files, inplace=True):
                match = re.search(p, line)
                if match:
                    line = line.replace(';', ' {get; set;}')
                sys.stdout.write(line)

    def get_files(self):
        path = os.path.dirname(os.path.abspath(__file__))
        if self.args.dir:
            path = self.args.dir
        if self.args.file:
            self.files = [os.path.join(path, i) for i in self.args.file.split(',')
                          if os.path.isfile(os.path.join(path, i))]
        elif self.args.startsWith:
            for i in os.listdir(path):
                if os.path.isfile(os.path.join(path, i)) and i.startswith(self.args.startsWith):
                    self.files.append(os.path.join(path, i))

        print 'Found files: %s' % str(self.files)