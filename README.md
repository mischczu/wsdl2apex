## wsdl2apex-mutator

### Scenario
Wsdl2apex is producing apex code from wsdl descriptor file. It produces two types of files, one containing model classes 
and other containig web service methods. Model classes have public members, but without getters and setters.

I've got scenario, in which I needed to visualise data from web service on Visual Force Page.

VF controller couldn't access class members from generated model classes, because it needed getters!

My model was big, so manually editing classes would be painful.
So I use Python to do it for me.

### Usage
```python
python wsdl2apex -f <comma separated files>
```

```python
python wsdl2apex -d <directory with files> -sw <file prefix>
```